package dev.duongnartist.enterprisedatamining.utilities;

/**
 * Created by duong on 7/9/16.
 */
public class ConsoleController {

    public static void println(String message) {
        System.out.println(message);
    }

    public static void println(int message) {
        System.out.println(message);
    }
}
