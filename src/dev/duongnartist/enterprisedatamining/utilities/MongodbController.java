package dev.duongnartist.enterprisedatamining.utilities;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * Created by duong on 7/7/16.
 */
public class MongodbController {

//    public static final String URI = "mongodb://duongnartist:07121994@ds021984.mlab.com:21984/enterprise";
    public static final String URI = "mongodb://localhost:27017/enterprise";
    private MongoClient mongoClient;
    private MongoDatabase mongoDatabase;

    public MongodbController(String connectionString) {
        MongoClientURI mongoClientURI = new MongoClientURI(connectionString);
        mongoClient = new MongoClient(mongoClientURI);
        mongoDatabase = mongoClient.getDatabase(mongoClientURI.getDatabase());
    }

    public MongoCollection<Document> getCollection(String collectionName) {
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collectionName);
        return mongoCollection;
    }
}
