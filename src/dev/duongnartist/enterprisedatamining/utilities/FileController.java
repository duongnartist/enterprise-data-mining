package dev.duongnartist.enterprisedatamining.utilities;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by duong on 7/7/16.
 */
public class FileController {
    public static String userDir = System.getProperty("user.dir");
    public static String lib = appendFolder(userDir, "lib");
    public static String res = appendFolder(userDir, "res");
    public static String plugins = appendFolder(res, "plugins");
    public static String corpus = appendFolder(res, "corpus");
    public static String countriesCorpus = appendFolder(corpus, "countries");

    public static String appendFolder(String rootFolder, String subFolder) {
        return rootFolder + File.separator + subFolder;
    }

    public static boolean deleteFile(String path) {
        File file = new File(path);
        if (file != null && file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static void writeStringToFile(String string, String file) {
        deleteFile(file);
        try {
            FileWriter writer = new FileWriter(file, true);
            if (writer != null) {
                writer.write(string);
                writer.close();
                ConsoleController.println("Đã ghi tệp: \"" + file + "\" kích thước " + string.length() + "!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readStringFromFile(String path) {
        String string = "";
        if (isExist(path)) {
            try {
                FileReader reader = new FileReader(path);
                if (reader != null) {
                    int character;
                    while ((character = reader.read()) != -1) {
                        string += (char) character;
                    }
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return string;
    }

    public static boolean isExist(String path) {
        File file = new File(path);
        if (file != null) {
            return file.exists();
        }
        return false;
    }
}
