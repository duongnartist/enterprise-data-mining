package dev.duongnartist.enterprisedatamining.utilities;

import gate.*;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.persist.PersistenceException;
import gate.util.GateException;
import gate.util.Out;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by duong on 7/11/16.
 */
public class GateController {

    private static GateController gateController = null;
    private static boolean gateInitialized = false;
    private static boolean corpusControllerInitialized = false;

    String gateHome = FileController.userDir;
    String pluginsHome = FileController.plugins;
    CorpusController corpusController = null;
    Corpus corpus = null;

    private GateController() {
        Gate.setGateHome(new File(gateHome));
        Gate.setPluginsHome(new File(pluginsHome));
    }

    public static GateController getGateController() {
        if (gateController == null) {
            gateController = new GateController();
        }
        return gateController;
    }

    public void initGate() {
        try {
            Gate.init();
            gateInitialized = true;
        } catch (GateException e) {
            e.printStackTrace();
        }
    }

    public void initCorpusController() {
        File pluginsHome = Gate.getPluginsHome();
        File anniePlugin = new File(pluginsHome, "ANNIE");
        File annieGapp = new File(anniePlugin, "ANNIE_with_defaults.gapp");
        try {
            corpusController =
                    (CorpusController) PersistenceManager.loadObjectFromFile(annieGapp);
            corpusControllerInitialized = true;
        } catch (PersistenceException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ResourceInstantiationException e) {
            e.printStackTrace();
        }
    }

    public static gate.Document createDocumentFrom(String text) {
        gate.Document document = null;
        try {
            document =  Factory.newDocument(text);
        } catch (ResourceInstantiationException e) {
            e.printStackTrace();
        }
        return document;
    }

    public static Corpus createNewCorpus(String corpusName) {
        Corpus corpus = null;
        try {
            corpus = Factory.newCorpus(corpusName);
        } catch (ResourceInstantiationException e) {
            e.printStackTrace();
        }
        return corpus;
    }

    public void setCorpus(Corpus corpus) {
        this.corpus = corpus;
        corpusController.setCorpus(corpus);
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public void execute() {
        if (corpusController != null) {
            try {
                corpusController.execute();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        } else {
            Out.println("Corpus controller is null");
        }
    }

    public ArrayList<String> getResult() {
        ArrayList<String> results = new ArrayList<>();
        Iterator iterator = corpus.iterator();
        while (iterator.hasNext()) {
            Document document = (Document) iterator.next();
            AnnotationSet defaultAnnotationSet = document.getAnnotations();
            Set annotationTypesRequired = new HashSet();
            annotationTypesRequired.add("EntProductName");
            annotationTypesRequired.add("EntProductQuantity");
            annotationTypesRequired.add("EntEnterpriseTurnover");
            annotationTypesRequired.add("EntEnterpriseName");
            Set<Annotation> annotationRequired =
                    new HashSet<Annotation>(defaultAnnotationSet.get(annotationTypesRequired));
            String xmlDocument = document.toXml(annotationRequired, true);
            StringBuffer buffer =new StringBuffer();
            buffer.append("<Enterprise>");
            buffer.append(xmlDocument);
            buffer.append("</Enterprise>");
            results.add(buffer.toString());
        }
        return results;
    }

    public static class SortedAnnotationList extends Vector {
        public SortedAnnotationList() {
            super();
        }

        public boolean addSortedExclusive(Annotation annotation) {
            Annotation currentAnnotation = null;
            for (int i=0; i<size(); ++i) {
                currentAnnotation = (Annotation) get(i);
                if(annotation.overlaps(currentAnnotation)) {
                    return false;
                }
            }
            long annotStart = annotation.getStartNode().getOffset().longValue();
            long currStart;
            for (int i=0; i < size(); ++i) {
                currentAnnotation = (Annotation) get(i);
                currStart = currentAnnotation.getStartNode().getOffset().longValue();
                if(annotStart < currStart) {
                    insertElementAt(annotation, i);
                    return true;
                }
            }
            int size = size();
            insertElementAt(annotation, size);
            return true;
        }
    }
}
