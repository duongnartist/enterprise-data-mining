package dev.duongnartist.enterprisedatamining.crawlers;

import gate.util.Out;
import org.apache.james.mime4j.dom.Body;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;

/**
 * Created by duong on 7/10/16.
 */
public class ExportEnterpriseCrawler extends BaseCrawler {

    ArrayList<String> vis = new ArrayList<>();
    ArrayList<String> ens = new ArrayList<>();

    public ExportEnterpriseCrawler() {
        super();
        sleep = 0;
        start = 1000;
        end = 9999;
        step = 1;
        formattedUrl = "http://dnxnk.moit.gov.vn/EntpDetail.asp?ID=%d&langs=1";
        generateUrls(false);
    }

    @Override
    public void crawl(String url, Document document) {
//        Out.println("-> " + url);
        Element body = document.body();
        Elements elements = body.select("table#AutoNumber1");
        if (elements.text().startsWith("Bộ Công Thương, 54 Hai Bà Trưng, Hà Nội Tel: 844 - 22202222     Fax: 844 - 22202525") == false) {
            Element viElement = elements.select("tbody").select("tr").select("td").select("p").get(0);
            Element enElement = elements.select("tbody").select("tr").select("td").select("p").get(1);
            String vi = viElement.text().trim().replace("  ", " ");
            String en = enElement.text().trim().replace("  ", " ");
            vis.add(vi);
            ens.add(en);
        }
    }

    @Override
    public void onFinish() {
        for (String vi: vis) {
            Out.println(vi);
        }
        Out.println("----");
        for (String en: ens) {
            Out.println(en);
        }
        String[] avis = null;
        vis.toArray(avis);
        avis = new HashSet<String>(Arrays.asList(avis)).toArray(new String[0]);
        String[] aens = null;
        vis.toArray(aens);
        avis = new HashSet<String>(Arrays.asList(avis)).toArray(new String[0]);
    }
}
