package dev.duongnartist.enterprisedatamining.crawlers;

import dev.duongnartist.enterprisedatamining.utilities.FileController;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by duong on 7/9/16.
 */
public class CountryCrawler extends BaseCrawler {

    public CountryCrawler() {
        super();
        sleep = 0;
        start = 1;
        end = 208;
        step = 1;
        formattedUrl = "http://vietnamexport.com/nuoc-lanh-tho/%d/tong-quan.html";
        generateUrls(true);
    }

    @Override
    public void crawl(String url, Document document) {
        Element body = document.body();
        Elements contentElements = body.select("div.wap-ct");
        if (contentElements != null) {
            Element titleElement = body.select("h1.name-country").first();
            String title = titleElement.text().replace("ĐẤT NƯỚC/LÃNH THỔ - ", "").trim().replace(" ", "_").toLowerCase();
            String path = FileController.appendFolder(FileController.countriesCorpus, title + ".txt");
            String content = contentElements.text();
            FileController.writeStringToFile(content, path);
        }
//        ConsoleController.println(rows.size());
//        for (Element element: rows) {
//            ConsoleController.println(element.text());
//        }
    }

    @Override
    public void onFinish() {

    }

}
