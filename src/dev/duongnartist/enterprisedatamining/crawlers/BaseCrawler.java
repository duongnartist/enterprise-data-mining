package dev.duongnartist.enterprisedatamining.crawlers;

import dev.duongnartist.enterprisedatamining.utilities.ConsoleController;
import dev.duongnartist.enterprisedatamining.utilities.JsoupController;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by duong on 7/9/16.
 */
public abstract class BaseCrawler extends Thread {

    protected long sleep = 1000;
    protected boolean running = false;
    protected String formattedUrl = "";
    protected int start = 0;
    protected int end = 0;
    protected int step = 0;
    protected ArrayList<String> urls = new ArrayList<String>();

    public BaseCrawler() {

    }

    public void generateUrls(boolean clear) {
        ConsoleController.println("Khởi tạo danh sách đường dẫn...");
        if (clear) {
            urls.clear();
        }
        for (int i = start; i <= end; i += step) {
            String url = String.format(formattedUrl, i);
            urls.add(url);
        }
        ConsoleController.println("Đã khởi tạo " + urls.size() + " đường dẫn!");
    }

    @Override
    public void run() {
        super.run();
        for (String url: urls) {
            if (running) {
                try {
                    Document document = JsoupController.fetchDocumentFromUrl(url);
                    crawl(url, document);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        onFinish();
    }

    public void startCrawl() {
        running = true;
        start();
        ConsoleController.println("Bắt đầu thu thập dữ liệu...");
    }

    public abstract void crawl(String url, Document document);
    public abstract void onFinish();
}
