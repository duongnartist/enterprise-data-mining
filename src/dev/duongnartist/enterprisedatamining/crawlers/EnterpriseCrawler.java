package dev.duongnartist.enterprisedatamining.crawlers;

import com.mongodb.client.MongoCollection;
import dev.duongnartist.enterprisedatamining.utilities.ConsoleController;
import dev.duongnartist.enterprisedatamining.utilities.FileController;
import dev.duongnartist.enterprisedatamining.utilities.JsoupController;
import dev.duongnartist.enterprisedatamining.utilities.MongodbController;
import gate.util.Out;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by duong on 7/9/16.
 */
public class EnterpriseCrawler extends BaseCrawler {

    private ArrayList<String> enterprises = new ArrayList<>();
    private MongodbController mongodbController = new MongodbController(MongodbController.URI);
    private MongoCollection<org.bson.Document> mongoCollection;

    public EnterpriseCrawler() {
        super();
        mongoCollection = mongodbController.getCollection("enterprises");
        sleep = 0;
        start = 2;
        end = 14015;
        step = 1;
        formattedUrl = "http://www.thongtincongty.com/?page=%d";
        urls.add("http://www.thongtincongty.com/");
        generateUrls(false);
    }

    @Override
    public void crawl(String url, Document document) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Element body = document.body();
                Elements resultElements = body.select("div.search-results");
                for (Element resultElement: resultElements) {
                    Element enterpriseElement = resultElement.select("a").first();
                    String href = enterpriseElement.attr("href");
                    String name = enterpriseElement.text();
                    enterprises.add(name);
//                    ConsoleController.println(name);
//                    ConsoleController.println(href);
                    try {
                        org.bson.Document enterprise = new org.bson.Document();
                        // id
                        String[] ids = href.split("/");
                        String entId = ids[ids.length - 1].split("-")[0];
                        ConsoleController.println(entId);
                        //
                        Document enterpriseDocument = JsoupController.fetchDocumentFromUrl(href);
                        Element bodyElement = enterpriseDocument.body();
                        // Name
                        String entName = enterpriseDocument.title().split(",")[0];
                        ConsoleController.println(entName);
                        Element element = bodyElement.select("div.jumbotron").first();
                        // Image
                        Element imageElement = element.select("img").first();
                        String entImage = imageElement.attr("src");
//                        ConsoleController.println(entImage);

                        element.select("a").remove();
                        element.select("h4").remove();
                        String entDetail = element.text();
                        // Put
                        enterprise.put("id", entId);
                        enterprise.put("name", entName);
                        enterprise.put("image", entImage);
                        enterprise.put("detail", entDetail);
                        mongoCollection.insertOne(enterprise);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                ConsoleController.println("Đã thu thập được: " + enterprises.size() + " tên doanh nghiệp!");
            }
        }).start();
    }

    @Override
    public void onFinish() {
//        ConsoleController.println("Tổng số: " + enterprises.size());
//        String content = "";
//        for (String ent: enterprises) {
//            content += ent + "\n";
//        }
//        String path = FileController.appendFolder(FileController.corpus, "enterprises.lst");
//        FileController.writeStringToFile(content, path);
    }

    public static void main(String[] args) {
        new EnterpriseCrawler().startCrawl();
    }

    class Enterprise extends org.bson.Document {

    }
}
