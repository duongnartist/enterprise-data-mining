package sample;

import dev.duongnartist.enterprisedatamining.utilities.FileController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import sample.models.DictionaryModel;

import java.util.ArrayList;

/**
 * Created by duong on 7/12/16.
 */
public class DictionaryTrainingEditorTab extends Tab {

    String gazetteer = FileController.appendFolder(FileController.res, "plugins");
    ArrayList<DictionaryModel> dictionaryModels = new ArrayList<>();
    Button button = new Button();
    BorderPane borderPane = new BorderPane();
    ListView<String> listView = new ListView<>();
    ObservableList<String> stringObservableList = FXCollections.observableArrayList();
    TextArea textArea = new TextArea();
    Label label = new Label();

    public DictionaryTrainingEditorTab() {
        gazetteer = FileController.appendFolder(gazetteer, "ANNIE");
        gazetteer = FileController.appendFolder(gazetteer, "resources");
        gazetteer = FileController.appendFolder(gazetteer, "gazetteer");
        initSubViews();
        initCoreData();
    }

    public void initSubViews() {
        initTab();
        initBorderPane();
        initLeft();
        initCenter();
        initBottom();
    }

    public void initTab() {
        setText("Huấn luyện từ điển");
        setClosable(false);
        setContent(borderPane);
    }

    public void initBorderPane() {
        borderPane.setPadding(new Insets(5, 5, 5, 5));
    }

    public void initLeft() {
        listView.setItems(stringObservableList);
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                textArea.setText(readContentFromFile(getCurrentPath()));
            }
        });
        borderPane.setLeft(listView);
    }

    public void initCenter() {
        borderPane.setCenter(textArea);
    }

    public void initBottom() {
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_RIGHT);
        hBox.setPadding(new Insets(5, 5, 5, 5));
        hBox.setSpacing(5);
        button.setText("Lưu");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                setDisableSubViews(true);
                String content = textArea.getText();
                writeContentToFile(content, getCurrentPath());
                setDisableSubViews(false);
            }
        });
        label.setText("Hãy chọn tệp từ điển, sửa chữa và lưu trữ.");
        hBox.getChildren().addAll(label, button);
        borderPane.setBottom(hBox);
        borderPane.setAlignment(borderPane.getBottom(), Pos.CENTER_RIGHT);
    }

    public void initCoreData() {
        dictionaryModels.add(new DictionaryModel("Lượng hàng", "ent_container_unit.lst"));
        dictionaryModels.add(new DictionaryModel("Tiền tệ", "ent_currency_unit.lst"));
        dictionaryModels.add(new DictionaryModel("Độ dài", "ent_length_unit.lst"));
        dictionaryModels.add(new DictionaryModel("Khối lượng", "ent_mass_unit.lst"));
        dictionaryModels.add(new DictionaryModel("Số lượng", "ent_quantity_unit.lst"));
        dictionaryModels.add(new DictionaryModel("Thể tích", "ent_volume_unit.lst"));
        dictionaryModels.add(new DictionaryModel("Tên doanh nghiệp", "ent_enterprise_name.lst"));
        dictionaryModels.add(new DictionaryModel("Tên mặt hàng", "ent_product_name.lst"));
        for (DictionaryModel dictionaryModel: dictionaryModels) {
            stringObservableList.add(dictionaryModel.filename);
        }
    }

    public String getCurrentPath() {
        return dictionaryModels.get(listView.getSelectionModel().getSelectedIndex()).path;
    }

    public String readContentFromFile(String filename) {
        String content = "";
        String path = FileController.appendFolder(gazetteer, filename);
        content = FileController.readStringFromFile(path);
        return content;
    }

    public void writeContentToFile(String content, String filename) {
        String path = FileController.appendFolder(gazetteer, filename);
        FileController.writeStringToFile(content, path);
    }

    public void setDisableSubViews(boolean disable) {
        listView.setDisable(disable);
        textArea.setDisable(disable);
        button.setDisable(disable);
    }

    public DictionaryTrainingEditorTab(String text) {
        super(text);
    }

    public DictionaryTrainingEditorTab(String text, Node content) {
        super(text, content);
    }


}
