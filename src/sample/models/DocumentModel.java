package sample.models;

import dev.duongnartist.enterprisedatamining.utilities.FileController;

import javax.print.Doc;
import java.io.File;

/**
 * Created by duong on 7/13/16.
 */
public class DocumentModel {

    private String content = "";
    private String path = "";
    private String taggedPath = "";
    private String taggedContent = "";
    private boolean tagged = false;
    private String name = "";

    public DocumentModel() {

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStyleContent() {
        return "<Enterprise style=\"font-family:verdana;\">" + content + "</Enterprise>";
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
        File file = new File(path);
        if (file != null && file.exists()) {
            content = FileController.readStringFromFile(path);
            name = file.getName().replace(".txt", "");
        }
        taggedPath = path.replace(".txt", ".xml");
        file = new File(taggedPath);
        if (file != null && file.exists()) {
            taggedContent = FileController.readStringFromFile(taggedPath);
            tagged = true;
        } else {
            tagged = false;
        }
    }

    public String getTaggedPath() {
        return taggedPath;
    }

    public void setTaggedPath(String taggedPath) {
        this.taggedPath = taggedPath;
    }

    public boolean isTagged() {
        return tagged;
    }

    public void setTagged(boolean tagged) {
        this.tagged = tagged;
    }

    public String getTaggedContent() {
        return taggedContent;
    }

    public void setTaggedContent(String taggedContent) {
        this.taggedContent = taggedContent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
