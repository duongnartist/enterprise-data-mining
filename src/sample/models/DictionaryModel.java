package sample.models;

/**
 * Created by duong on 7/11/16.
 */
public class DictionaryModel {
    public String filename = "";
    public String path = "";
    public String content = "";

    public DictionaryModel(String filename, String path) {
        this.filename = filename;
        this.path = path;
    }
}
