package sample;

import dev.duongnartist.enterprisedatamining.utilities.FileController;
import dev.duongnartist.enterprisedatamining.utilities.GateController;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.util.HashMap;
import java.util.stream.Stream;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Trích rút thông tin xuất nhập khẩu");
        Group root = new Group();
        Scene scene = new Scene(root, 800, 600, Color.WHITE);
        TabPane tabPane = new TabPane();
        BorderPane mainPane = new BorderPane();
        Label label = new Label("Đang khởi tạo...");
        mainPane.setCenter(label);
        GateController.getGateController().initGate();
        tabPane.getTabs().add(new DictionaryTrainingEditorTab());
        tabPane.getTabs().add(new InformationExtractionTab());
        mainPane.prefHeightProperty().bind(scene.heightProperty());
        mainPane.prefWidthProperty().bind(scene.widthProperty());
        mainPane.setCenter(tabPane);
        root.getChildren().add(mainPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }



}
