package sample;

import dev.duongnartist.enterprisedatamining.utilities.FileController;
import dev.duongnartist.enterprisedatamining.utilities.GateController;
import dev.duongnartist.enterprisedatamining.utilities.JsoupController;
import gate.Corpus;
import gate.creole.annic.apache.lucene.document.Document;
import gate.creole.annic.apache.lucene.util.StringHelper;
import gate.util.Out;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sample.models.DocumentModel;

import javax.print.Doc;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by duong on 7/13/16.
 */
public class InformationExtractionTab extends Tab {

    static Color RED = Color.valueOf("#FF2A68");
    static Color ORANGE = Color.valueOf("#FF5E3A");
    static Color GREEN = Color.valueOf("#0BD318");
    static Color BLUE = Color.valueOf("#1D62F0");
    static int RADIUS = 5;
    static int EDGE = 10;

    BorderPane borderPane = new BorderPane();
    ListView<String> documentListView = new ListView<>();
    TextField urlTextField = new TextField();
    Button addDocButton = new Button("Thêm văn bản từ tệp");
    Button delDocButton = new Button("Xóa văn bản");
    Button addUrlDocButton = new Button("Thêm văn bản từ web");
    WebView webView = new WebView();
    ObservableList<String> documentStrings = FXCollections.observableArrayList();
    ArrayList<DocumentModel> documentModels = new ArrayList<>();
    TreeView<String> nerTreeView = new TreeView<>();
    TreeItem<String> nerTreeItem = new TreeItem<>("Thực thể văn bản");
    String[] nerNames = {"Doanh Nghiệp", "Kim Ngạch", "Sản Phẩm", "Lượng Hàng"};
    Button executeButton = new Button("Trích rút");

    ArrayList<String> enterpriseNames = new ArrayList<String>();
    ArrayList<String> enterpriseTurnovers = new ArrayList<String>();
    ArrayList<String> productNames = new ArrayList<String>();
    ArrayList<String> productQuantities = new ArrayList<String>();

    Circle[] nerCircles = {
            new Circle(0, 0, RADIUS, RED),
            new Circle(0, 0, RADIUS, ORANGE),
            new Circle(0, 0, RADIUS, GREEN),
            new Circle(0, 0, RADIUS, BLUE)
    };

    Rectangle[] nerRectangles = {
            new Rectangle(EDGE, EDGE, RED),
            new Rectangle(EDGE, EDGE, ORANGE),
            new Rectangle(EDGE, EDGE, GREEN),
            new Rectangle(EDGE, EDGE, BLUE)
    };

    DocumentModel documentModel = null;

    public InformationExtractionTab() {
        initCoreData();
        initTab();
        initSubViews();
    }

    private void initTab() {
        setText("Trích rút thông tin");
        setClosable(false);
        setTooltip(new Tooltip("Chức năng trích rút thông tin từ văn bản"));
    }

    private void initSubViews() {
        initBorderPane();
    }

    private void initBorderPane() {
        setContent(borderPane);
        initLeftBorderPane();
        initRightBorderPane();
        initCenterBorderPane();
        initTopBorderPane();
        initBottomBorderPane();
    }

    private void initBottomBorderPane() {
        executeButton.setPrefWidth(200);
        executeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                executeButtonHandle();
            }
        });
        addDocButton.setPrefWidth(200);
        addDocButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                addDocButtonHandle();
            }
        });
        delDocButton.setPrefWidth(200);
        delDocButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                delDocButtonHandle();
            }
        });
        addUrlDocButton.setPrefWidth(200);
        addUrlDocButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                displayDocumentFromUrl();
            }
        });
        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(5, 5, 5, 5));
        hBox.setSpacing(5);
        hBox.getChildren().addAll(addUrlDocButton, addDocButton, delDocButton, executeButton);
        borderPane.setBottom(hBox);
    }

    private void executeButtonHandle() {
        GateController.getGateController().initCorpusController();
        Corpus corpus = GateController.createNewCorpus("StandAloneAnnie corpus");
        for (DocumentModel documentModel: documentModels) {
            gate.Document document = GateController.createDocumentFrom(documentModel.getContent());
            corpus.add(document);
        }
        GateController.getGateController().setCorpus(corpus);
        GateController.getGateController().execute();
        ArrayList<String> results = GateController.getGateController().getResult();
        reInitNers();
        for (int i = 0; i < results.size(); i++) {
            String result = results.get(i);
            result = result.replace("<Enterprise", "<Enterprise style=\"font-family:verdana;\"");
            result = result.replace("<EntEnterpriseName", "<EntEnterpriseName style=\"color:white;background-color:#FF2A68;\"");
            result = result.replace("<EntEnterpriseTurnover", "<EntEnterpriseTurnover style=\"color:white;background-color:#FF5E3A;\"");
            result = result.replace("<EntProductName", "<EntProductName style=\"color:white;background-color:#0BD318;\"");
            result = result.replace("<EntProductQuantity", "<EntProductQuantity style=\"color:white;background-color:#1D62F0;\"");
            parseResults(result);
            DocumentModel documentModel = documentModels.get(i);
            documentModel.setTaggedContent(result);
            documentModel.setTagged(true);
            FileController.writeStringToFile(documentModel.getTaggedContent(), documentModel.getTaggedPath());
        }
        setNerTreeItems();
    }

    private void reInitNers() {
        enterpriseNames = new ArrayList<>();
        enterpriseTurnovers = new ArrayList<>();
        productNames = new ArrayList<>();
        productQuantities = new ArrayList<>();
    }

    private void parseResults(String result) {
        org.jsoup.nodes.Document document1 = Jsoup.parse(result);
        Element body = document1.body();
        Elements names = body.select("EntEnterpriseName");
        if (names != null) {
            for (Element element: names) {
                enterpriseNames.add(element.text());
            }
        }
        Elements enterpriseTurnoverElements = body.select("EntEnterpriseTurnover");
        if (enterpriseTurnoverElements != null) {
            for (Element element: enterpriseTurnoverElements) {
                enterpriseTurnovers.add(element.text());
            }
        }
        Elements productNameElements = body.select("EntProductName");
        if (productNameElements != null) {
            for (Element element: productNameElements) {
                productNames.add(element.text());
            }
        }
        Elements productQuantityElements = body.select("EntProductQuantity");
        if (productQuantityElements != null) {
            for (Element element: productQuantityElements) {
                productQuantities.add(element.text());
            }
        }
    }

    private void initRightBorderPane() {
        nerTreeItem.setExpanded(true);
        nerTreeView.setRoot(nerTreeItem);
        borderPane.setRight(nerTreeView);
    }

    private void setNerTreeItems() {
        nerTreeItem = new TreeItem<>("Thực thể văn bản");
        nerTreeItem.setExpanded(true);
        TreeItem<String> enterpriseNameTreeItem = new TreeItem<String> (String.format("%s (%d)", nerNames[0], enterpriseNames.size()), nerCircles[0]);
        for (String enterpriseName: enterpriseNames) {
            enterpriseNameTreeItem.getChildren().add(new TreeItem<>(enterpriseName, new Rectangle(EDGE, EDGE, RED)));
        }
        nerTreeItem.getChildren().add(enterpriseNameTreeItem);
        TreeItem<String> enterpriseTurnoverTreeItem = new TreeItem<String> (String.format("%s (%d)", nerNames[1], enterpriseTurnovers.size()), nerCircles[1]);
        for (String enterpriseTurnover: enterpriseTurnovers) {
            enterpriseTurnoverTreeItem.getChildren().add(new TreeItem<>(enterpriseTurnover, new Rectangle(EDGE, EDGE, ORANGE)));
        }
        nerTreeItem.getChildren().add(enterpriseTurnoverTreeItem);
        TreeItem<String> productNameTreeItem = new TreeItem<String> (String.format("%s (%d)", nerNames[2], productNames.size()), nerCircles[2]);
        for (String productName: productNames) {
            productNameTreeItem.getChildren().add(new TreeItem<>(productName, new Rectangle(EDGE, EDGE, GREEN)));
        }
        nerTreeItem.getChildren().add(productNameTreeItem);
        TreeItem<String> productQuantityTreeItem = new TreeItem<String> (String.format("%s (%d)", nerNames[3], productQuantities.size()), nerCircles[3]);
        for (String productQuantity: productQuantities) {
            productQuantityTreeItem.getChildren().add(new TreeItem<>(productQuantity, new Rectangle(EDGE, EDGE, BLUE)));
        }
        nerTreeItem.getChildren().add(productQuantityTreeItem);
        nerTreeView.setRoot(nerTreeItem);
    }

    private void initTopBorderPane() {
        urlTextField.setTooltip(new Tooltip("Nhập đường dẫn web tại đây..."));
        borderPane.setTop(urlTextField);
    }

    private void displayDocumentFromUrl() {
        String url = urlTextField.getText();
        urlTextField.setText("");
        if (url.length() > 0) {
            try {
                org.jsoup.nodes.Document document = JsoupController.fetchDocumentFromUrl(url);
                String name = System.currentTimeMillis() + "";
                Element body = document.body();
                DocumentModel documentModel = new DocumentModel();
                documentModel.setPath(FileController.appendFolder(FileController.corpus, name + ".txt"));
                documentModel.setName(name);
                documentModel.setContent(body.text());
                FileController.writeStringToFile(documentModel.getContent(), documentModel.getPath());
                documentModels.add(documentModel);
                documentStrings.add(documentModel.getName());
                webView.getEngine().loadContent(documentModel.getStyleContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initCenterBorderPane() {
        borderPane.setCenter(webView);
    }

    private void initLeftBorderPane() {
        documentListView.setItems(documentStrings);
        documentListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                displayDocumentFromFile();
            }
        });
        borderPane.setLeft(documentListView);
    }

    private void addDocButtonHandle() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Chọn một tệp văn bản...");
        fileChooser.setInitialDirectory(new File(FileController.userDir));
        List<File> files = fileChooser.showOpenMultipleDialog(null);
        if (files != null) {
            for (File file : files) {
                DocumentModel documentModel = new DocumentModel();
                documentModel.setPath(file.getAbsolutePath());
                FileController.writeStringToFile(documentModel.getContent(), FileController.appendFolder(FileController.corpus, file.getName()));
                documentModels.add(documentModel);
                documentStrings.add(documentModel.getName());
                reInitNers();
                setNerTreeItems();
            }
        }
    }

    private void delDocButtonHandle() {
        int index = getSelectedIndexDocument();
        documentModel = documentModels.get(index);
        if (documentModel != null) {
            FileController.deleteFile(documentModel.getPath());
            FileController.deleteFile(documentModel.getTaggedPath());
            webView.getEngine().loadContent("");
            reInitNers();
            setNerTreeItems();
            documentModels.remove(index);
            documentStrings.remove(index);
            documentModel = null;
        }
    }

    private void displayDocumentFromFile() {
        int index = getSelectedIndexDocument();
        if (index >= 0) {
            DocumentModel documentModel = documentModels.get(index);
            if (documentModel != null) {
                if (documentModel.isTagged()) {
                    webView.getEngine().loadContent(documentModel.getTaggedContent());
                    reInitNers();
                    parseResults(documentModel.getTaggedContent());
                    setNerTreeItems();
                } else {
                    webView.getEngine().loadContent(documentModel.getStyleContent());
                }
            }
        }
    }

    private int getSelectedIndexDocument() {
        return documentListView.getSelectionModel().getSelectedIndex();
    }

    private void initCoreData() {
        fetchDocumentFiles();
    }

    private void fetchDocumentFiles() {
        documentModel = null;
        documentStrings = FXCollections.observableArrayList();
        String[] filenames = {};
        File file = new File(FileController.corpus);
        if (file != null && file.exists()) {
            filenames = file.list();
            for (String filename: filenames) {
                if (filename.contains(".txt")) {
                    DocumentModel documentModel = new DocumentModel();
                    documentModel.setPath(FileController.appendFolder(FileController.corpus, filename));
                    documentModels.add(documentModel);
                    documentStrings.add(documentModel.getName());
                }
            }
        }
        documentListView.setItems(documentStrings);
    }

    public InformationExtractionTab(String text) {
        super(text);
    }

    public InformationExtractionTab(String text, Node content) {
        super(text, content);
    }
}
